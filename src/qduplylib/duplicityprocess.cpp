/*
    Copyright © Andrii Pravorskyi 2017

    This file is part of QDuply.

    QDuply is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    QDuply is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QDuply.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "duplicityprocess.h"


DuplicityProcess::DuplicityProcess(const Job & job_, QObject * parent) :
    QProcess(parent),
    job(job_),
    logFilename(std::tmpnam(nullptr))
{
#warning check under Windows
/*
#ifdef Q_OS_WIN32
    //take first letter - disk drive
    QChar disk = _logFilename.at(0);
    logFilename_.replace(0, 2, QLatin1String("/cygdrive/")+disk.toLower()); // replace 'X:' to '/cygdrive/x'
#endif
*/
}
