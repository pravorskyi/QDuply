/*
    Copyright © Andrii Pravorskyi 2017

    This file is part of QDuply.

    QDuply is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    QDuply is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QDuply.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "job.h"


Job::Job(BackupPtr& _backup, StoragePtr& _storage) :
    backup(_backup),
    storage(_storage)
{
}


void Job::setGlobalDuplicityOptions(const DuplicityOptionsPtr &opts)
{
    duplicity_options->insert(opts->begin(), opts->end());
}


void Job::setGlobalGpgOptions(const GpgOptionsPtr &opts)
{
    gpg_options->insert(opts->begin(), opts->end());
}


const std::string& Job::typeStr(const Job::Type & type)
{
    static const std::vector<std::string> vec = {
        "",                   // BackupAuto
        "full",               // BackupFull
        "incremental",        // BackupIncremental
        "collection-status",  // CollectionStatus
        "list-current-files", // ListCurrentFiles,
        "verify"              // Verify
    };

    return vec[type];
}
