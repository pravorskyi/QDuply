/*
    Copyright © Andrii Pravorskyi 2017

    This file is part of QDuply.

    QDuply is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    QDuply is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QDuply.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "qduplyconfig.h"
#include "structs.h"
#include <confuse.h>
#include <fstream>
#include <iostream>
#include <sstream>


using BackupMap  = std::map<std::string, BackupPtr>;
using StorageMap = std::map<std::string, StoragePtr>;


#define GENERATE_CFG_OPTS                                                                        \
                                                                                                 \
cfg_opt_t gpg_opts[] = {                                                                         \
    CFG_INT("bzip2_compress_level", -1, CFGF_NONE),                                              \
    CFG_STR("compress_algo",         0, CFGF_NONE),                                              \
    CFG_STR("encrypt_key",           0, CFGF_NONE),                                              \
    CFG_STR("sign_key",              0, CFGF_NONE),                                              \
    CFG_STR("sign_password",         0, CFGF_NONE),                                              \
    CFG_STR("password",              0, CFGF_NONE),                                              \
    CFG_END()                                                                                    \
};                                                                                               \
                                                                                                 \
cfg_opt_t duplicity_opts[] = {                                                                   \
    CFG_BOOL("asynchronous-upload", cfg_false, CFGF_NONE),                                       \
    CFG_STR("verbosity",            0,         CFGF_NONE),                                       \
    CFG_INT("volsize",              0,         CFGF_NONE),                                       \
    CFG_END()                                                                                    \
};                                                                                               \
                                                                                                 \
cfg_opt_t backup_opts[] = {                                                                      \
    CFG_STR("path",              0,              CFGF_NONE),                                     \
    CFG_SEC("duplicity_options", duplicity_opts, CFGF_NONE),                                     \
    CFG_SEC("gpg_options",       gpg_opts,       CFGF_NONE),                                     \
    CFG_END()                                                                                    \
};                                                                                               \
                                                                                                 \
cfg_opt_t storage_opts[] = {                                                                     \
    CFG_STR("path",              0,              CFGF_NONE),                                     \
    CFG_SEC("duplicity_options", duplicity_opts, CFGF_NONE),                                     \
    CFG_SEC("gpg_options",       gpg_opts,       CFGF_NONE),                                     \
    CFG_END()                                                                                    \
};                                                                                               \
                                                                                                 \
cfg_opt_t job_opts[] = {                                                                         \
    CFG_STR("backup",            0,              CFGF_NONE),                                     \
    CFG_STR("storage",           0,              CFGF_NONE),                                     \
    CFG_STR("subdir",            "",             CFGF_NONE),                                     \
    CFG_SEC("duplicity_options", duplicity_opts, CFGF_NONE),                                     \
    CFG_SEC("gpg_options",       gpg_opts,       CFGF_NONE),                                     \
    CFG_END()                                                                                    \
};                                                                                               \
                                                                                                 \
cfg_opt_t root_opts[] = {                                                                        \
    CFG_SEC("backup",            backup_opts,    CFGF_MULTI | CFGF_TITLE | CFGF_NO_TITLE_DUPES), \
    CFG_SEC("storage",           storage_opts,   CFGF_MULTI | CFGF_TITLE | CFGF_NO_TITLE_DUPES), \
    CFG_SEC("job",               job_opts,       CFGF_MULTI | CFGF_TITLE | CFGF_NO_TITLE_DUPES), \
    CFG_SEC("duplicity_options", duplicity_opts, CFGF_NONE),                                     \
    CFG_SEC("gpg_options",       gpg_opts,       CFGF_NONE),                                     \
    CFG_END()                                                                                    \
};                                                                                               \



void error_handler(cfg_t *cfg, const char *format, va_list args)
{
    if (cfg && cfg->filename) {
        std::cerr << "[ERROR] CFG: File " << ": " << cfg->filename;

        if (cfg->filename)
            std::cerr << ", line: " << cfg->line;

        std::cerr << '\n';
    }

    vprintf(format, args);
}


// callback for validating "duplicity_options" section
int cb_validate_duplicity_options(cfg_t *cfg, cfg_opt_t *opt)
{
    auto size = cfg_size(cfg, "duplicity_options");
    for(auto i = 0u; i < size; ++i) {
        auto sec = cfg_opt_getnsec(opt, i);

        if(!sec) {
            cfg_error(cfg, "%s: section is NULL.", __func__);
            return -1;
        }
        //TODO: add validation option "asynchronous-upload"
        //TODO: add validation option "verbosity"
        //TODO: add validation option "volsize"
    }
    return 0;
}


//callback for validating "gpg_options" section
int cb_validate_gpg_options(cfg_t *cfg, cfg_opt_t *opt)
{
    auto size = cfg_size(cfg, "gpg_options");
    for(auto i = 0u; i < size; ++i) {
        auto sec = cfg_opt_getnsec(opt, i);

        if(!sec) {
            cfg_error(cfg, "%s: section is NULL.", __func__);
            return -1;
        }
        //TODO: add validation option "bzip2_compress_level"
        //TODO: add validation option "compress_algo"
        //TODO: add validation option "encrypt_key"
        //TODO: add validation option "sign_key"
        //TODO: add validation option "sign_password"
        //TODO: add validation option "password"
    }
    return 0;
}


// callback for validating "backup" section
int cb_validate_backup(cfg_t *cfg, cfg_opt_t *opt)
{
    auto size = cfg_size(cfg, "backup");
    for(auto i = 0u; i < size; ++i) {
        auto sec = cfg_opt_getnsec(opt, i);

        if(!sec) {
            cfg_error(cfg, "%s: section is NULL.", __func__);
            return -1;
        }

        if(cfg_getstr(sec, "path") == 0) {
            cfg_error(cfg, "\"path\" option must be set for \"backup\" \"%s\"", cfg_title(sec));
            return -1;
        }
    }
    return 0;
}


//callback for validating "storage" section
int cb_validate_storage(cfg_t *cfg, cfg_opt_t *opt)
{
    auto size = cfg_size(cfg, "storage");
    for(auto i = 0u; i < size; ++i) {
        auto sec = cfg_opt_getnsec(opt, i);

        if(!sec) {
            cfg_error(cfg, "%s: section is NULL.", __func__);
            return -1;
        }

        if(cfg_getstr(sec, "path") == 0) {
            cfg_error(cfg, "\"path\" option must be set for \"storage\" \"%s\"", cfg_title(sec));
            return -1;
        }
    }
    return 0;
}


// callback for validating "job" section
int cb_validate_job(cfg_t *cfg, cfg_opt_t *opt)
{
    auto size = cfg_size(cfg, "job");
    for(auto i = 0u; i < size; ++i) {
        auto sec = cfg_opt_getnsec(opt, i);

        if(!sec) {
            cfg_error(cfg, "%s: section is NULL.", __func__);
            return -1;
        }

        if(cfg_getstr(sec, "backup") == 0) {
            cfg_error(cfg, "\"backup\" option must be set for \"job\" \"%s\"", cfg_title(sec));
            return -1;
        }

        if(cfg_getstr(sec, "storage") == 0) {
            cfg_error(cfg, "\"storage\" option must be set for \"storage\" \"%s\"", cfg_title(sec));
            return -1;
        }

        if(cfg_gettsec(cfg, "backup", cfg_getstr(sec, "backup")) == 0) {
            cfg_error(cfg, "\"%s\" backup not found in job \"%s\"", cfg_getstr(sec, "backup"), cfg_title(sec));
            return -1;
        }

        if(cfg_gettsec(cfg, "storage", cfg_getstr(sec, "storage")) == 0) {
            cfg_error(cfg, "\"%s\" storage not found in job \"%s\"", cfg_getstr(sec, "storage"), cfg_title(sec));
            return -1;
        }
    }
    return 0;
}


DuplicityOptionsPtr parseDuplicityOptions(cfg_t* const cfg)
{
    auto opts = std::make_shared<DuplicityOptions>();

    if(cfg_size(cfg, "duplicity_options") != 1)
        return opts;

    auto dup_sec = cfg_getsec(cfg, "duplicity_options");

    if (cfg_size(dup_sec, "verbosity") == 1)
        (*opts)["verbosity"] = cfg_getstr(dup_sec, "verbosity");

    if (cfg_size(dup_sec, "volsize") == 1 && cfg_getint(dup_sec, "volsize") != 0)
        (*opts)["volsize"] = QVariant::fromValue(cfg_getint(dup_sec, "volsize"));

    return opts;
}


GpgOptionsPtr parseGpgOptions(cfg_t * const cfg)
{
    auto opts = std::make_shared<GpgOptions>();

    if(cfg_size(cfg, "gpg_options") != 1)
        return opts;

    auto gpg_sec = cfg_getsec(cfg, "gpg_options");

    if (cfg_size(gpg_sec, "bzip2_compress_level") == 1 && cfg_getint(gpg_sec, "bzip2_compress_level") != 0)
        (*opts)["bzip2_compress_level"] = QVariant::fromValue(cfg_getint(gpg_sec, "bzip2_compress_level"));

    if (cfg_size(gpg_sec, "compress_algo") == 1)
        (*opts)["compress_algo"] = cfg_getstr(gpg_sec, "compress_algo");

    if (cfg_size(gpg_sec, "encrypt_key") == 1)
        (*opts)["encrypt_key"] = cfg_getstr(gpg_sec, "encrypt_key");

    if (cfg_size(gpg_sec, "sign_key") == 1)
        (*opts)["sign_key"] = cfg_getstr(gpg_sec, "sign_key");

    if (cfg_size(gpg_sec, "sign_password") == 1)
        (*opts)["sign_password"] = cfg_getstr(gpg_sec, "sign_password");

    if (cfg_size(gpg_sec, "password") == 1)
        (*opts)["password"] = cfg_getstr(gpg_sec, "password");

    return opts;
}


BackupsPtr parseBackups(cfg_t * const cfg, BackupMap &backupMap)
{
    auto size = cfg_size(cfg, "backup");
    auto backups = std::make_shared<Backups>();
    backups->reserve(size);

    for(auto i = 0u; i < size; ++i) {
        auto backup  = std::make_shared<Backup>();
        auto bk_sec  = cfg_getnsec(cfg, "backup", i);
        backup->name = cfg_title(bk_sec);
        backup->path = cfg_getstr(bk_sec, "path");

        backup->duplicity_options = parseDuplicityOptions(bk_sec);
        backup->gpg_options       = parseGpgOptions(bk_sec);

        backups->push_back(backup);
        backupMap[backup->name] = backup;
    }

    return backups;
}


StoragesPtr parseStorages(cfg_t*const cfg, StorageMap &storageMap)
{
    auto size = cfg_size(cfg, "storage");
    auto storages = std::make_shared<Storages>();
    storages->reserve(size);

    for(auto i = 0u; i < size; ++i) {
        StoragePtr storage = std::make_shared<Storage>();
        cfg_t *storage_sec = cfg_getnsec(cfg, "storage", i);
        storage->name = cfg_title(storage_sec);
        storage->path = cfg_getstr(storage_sec, "path");

        storage->duplicity_options = parseDuplicityOptions(storage_sec);
        storage->gpg_options       = parseGpgOptions(storage_sec);

        storages->push_back(storage);
        storageMap[storage->name] = storage;
    }
    return storages;
}


JobsPtr parseJobs(cfg_t*const cfg, const BackupMap &backupMap, const StorageMap &storageMap)
{
    auto size = cfg_size(cfg, "job");
    auto jobs = std::make_shared<Jobs>();
    jobs->reserve(size);

    for(auto i = 0u; i < size; ++i) {
        auto job_sec = cfg_getnsec(cfg, "job", i);
        auto backup  = backupMap.at(cfg_getstr(job_sec, "backup"));
        auto storage = storageMap.at(cfg_getstr(job_sec, "storage"));
        auto job     = std::make_shared<Job>(backup, storage);
        job->name    = cfg_title(job_sec);
        job->subdir  = cfg_getstr(job_sec, "subdir");

        job->duplicity_options = parseDuplicityOptions(job_sec);
        job->gpg_options       = parseGpgOptions(job_sec);

        jobs->push_back(job);
    }
    return jobs;
}


bool QDuplyConfig::read(const std::string & filename)
{
    GENERATE_CFG_OPTS
    auto cfg = cfg_init(root_opts, CFGF_NONE);
    cfg_set_error_function(cfg, error_handler);

    // set a validating callback function for sections
    cfg_set_validate_func(cfg, "duplicity_options", &cb_validate_duplicity_options);
    cfg_set_validate_func(cfg, "gpg_options", &cb_validate_gpg_options);
    cfg_set_validate_func(cfg, "backup", &cb_validate_backup);
    cfg_set_validate_func(cfg, "storage", &cb_validate_storage);
    cfg_set_validate_func(cfg, "job", &cb_validate_job);  

    auto ret = cfg_parse(cfg, filename.c_str());

    if( ret == CFG_FILE_ERROR ) {
        error_ = "Configuration file \"" + filename + "\" could not be read: " + strerror(errno);
        return false;
    } else if( ret == CFG_PARSE_ERROR ) {
        error_ = "Configuration file \"" + filename + "\" could not be parsed: " + strerror(errno);
        return false;
    }

    globalDuplicityOptions = parseDuplicityOptions(cfg);
    globalGpgOptions       = parseGpgOptions(cfg);

    BackupMap  backupMap;
    backups  = parseBackups(cfg, backupMap);

    StorageMap storageMap;
    storages = parseStorages(cfg, storageMap);

    jobs     = parseJobs(cfg, backupMap, storageMap);

    cfg_free(cfg);
    return true;
}


bool QDuplyConfig::write(const std::string & filename)
{
    GENERATE_CFG_OPTS
    auto cfg = cfg_init(root_opts, CFGF_NONE);
    cfg_set_error_function(cfg, error_handler);

    // set a validating callback function for sections
    cfg_set_validate_func(cfg, "duplicity_options", &cb_validate_duplicity_options);
    cfg_set_validate_func(cfg, "gpg_options", &cb_validate_gpg_options);
    cfg_set_validate_func(cfg, "backup", &cb_validate_backup);
    cfg_set_validate_func(cfg, "storage", &cb_validate_storage);
    cfg_set_validate_func(cfg, "job", &cb_validate_job);

}


std::string QDuplyConfig::errorMessage() const
{
    return error_;
}
