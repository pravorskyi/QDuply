/*
    Copyright © Andrii Pravorskyi 2017

    This file is part of QDuply.

    QDuply is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    QDuply is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QDuply.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include "job.h"
#include "structs.h"
struct cfg_t;

/*!
 * \class QSqlDatabase
 * \brief The QDuplyConfig class provides API for reading and writing configuration.
 *
 * Before using method "write" set the members "backups", "jobs", "storages" and "globalOptions";
 * Example:
 * \snippet QDuplyConfig cfg;
 * cfg.backups       = currentBackups;
 * cfg.jobs          = currentJobs;
 * cfg.storages      = currentStorages;
 * cfg.globalOptions = currentGlobalOptions;
 * cfg.write("test.conf");
 */

class QDuplyConfig
{
public:
    bool        read(const std::string & filename);
    bool        write(const std::string & filename);
    std::string errorMessage() const;

    BackupsPtr          backups;
    JobsPtr             jobs;
    StoragesPtr         storages;
    DuplicityOptionsPtr globalDuplicityOptions;
    GpgOptionsPtr       globalGpgOptions;
private:
    std::string error_;
};
