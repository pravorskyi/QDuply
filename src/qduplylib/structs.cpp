/*
    Copyright © Andrii Pravorskyi 2017

    This file is part of QDuply.

    QDuply is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    QDuply is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QDuply.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "structs.h"
#include <set>


//TODO: make constexpr in future
const std::set<std::string> & AvailableDuplicityOptions()
{
    static const std::set<std::string> opts = {
        "async",
        "duplicity-options"
        "verbosity",
        "volsize",
    };

    return opts;
}


//TODO: make constexpr in future
const std::set<std::string> & AvailableGpgOptions()
{
    static const std::set<std::string> opts = {
        "bzip2_compress_level",
        "compress_algo",
        "encrypt_key",
        "sign_key",
        "sign_password",
        "password"
    };

    return opts;
}
