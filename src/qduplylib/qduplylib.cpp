/*
    Copyright © Andrii Pravorskyi 2017

    This file is part of QDuply.

    QDuply is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    QDuply is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QDuply.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "qduplylib.h"
#include "structs.h"
#include "qduplyconfig.h"
#include "duplicityprocess.h"

#include <QtCore/QCoreApplication>
#include <QtCore/QDebug>
#include <QtCore/QDir>
#include <QtCore/QRegularExpression>
#include <QtCore/QStringBuilder>
#include <cassert>
#include <iostream>
#include <sstream>


QDuplyLib::QDuplyLib(QObject* parent) :
    QObject(parent)
{
}


bool QDuplyLib::init()
{
#ifdef Q_OS_WIN32
    DUPLICITY_CMD = '"' + QCoreApplication::applicationDirPath().replace('/', '\\') + QString("\\cygwin\\bin\\bash.exe\"");
#endif

    QDuplyConfig conf;

    if(!conf.read(configPath_)) {
        error_ = "Read config failed: " + conf.errorMessage();
        emit sigInitFinished(false);
        return false;
    }

    backups_  = conf.backups;
    storages_ = conf.storages;
    jobs_     = conf.jobs;

    for(const auto &j : *jobs_.get()) {
        j->setGlobalDuplicityOptions(conf.globalDuplicityOptions);
        j->setGlobalGpgOptions(conf.globalGpgOptions);
    }

    emit sigInitFinished(true);
    return true;
}


void QDuplyLib::setConfigPath(const std::string& path)
{
    configPath_ = path;
}


QStringList QDuplyLib::generateArgsJob(const JobPtr& job, const Job::Type &jobType, DuplicityProcess* const) const
{
    QStringList args;

#ifdef Q_OS_WIN
    // generate Cygwin-style path
    std::string runPath = pathWin2Cygwin(qApp->applicationDirPath()).toStdString() + "/run.sh";
    args << runPath;
#endif

    args << QString::fromStdString(Job::typeStr(jobType));

    //Generate exclude list
    auto exclude_opt = job->duplicity_options->find("exclude");
    if(exclude_opt != job->duplicity_options->cend()) {
        std::vector<std::string> exclude_list = qvariant_cast<std::vector<std::string>>(exclude_opt->second);
        for(const auto &pattern : exclude_list)
            args << QString::fromStdString("--exclude=" + job->backup->path + pattern);
    }

    QStringList gpgOptions;

    for(auto const& it : *job->gpg_options) {
        qDebug() << it.second.type();
 
/*        switch(it.key()) {
        case O_Async:
            args << "--asynchronous-upload";
            break;
        case O_GpgBzip2CompressLevel:
            gpgOptions << QLatin1String("--bzip2-compress-level=") % QString::number(it.value().toInt());
            break;
        case O_GpgCompressAlgo:
            gpgOptions << QLatin1String("--compress-algo=") % it.value().toString();
            break;
        case O_GpgEncryptKey:
            args << QLatin1String("--encrypt-key=") % it.value().toString();
            break;
        case O_GpgPassword:
            break;
        case O_GpgSignKey:
            args << QLatin1String("--sign-key=") % it.value().toString();
            break;
        case O_GpgSignPassword:
            break;
        case O_Verbosity:
            args << QLatin1String("--verbosity=") % it.value().toString();
            break;
        case O_Volzise: {
            if(jobType == Job::Type::Verify || jobType == Job::Type::ListCurrentFiles )
                break;
            args << QLatin1String("--volsize=") % QString::number(it.value().toInt());
            break;
        }
*/        
    }

    if(!gpgOptions.isEmpty())
        args << "--gpg-options="+ gpgOptions.join(QLatin1Char(' '));

    //FIXME: windows cygwin problem
    //args << QString("--log-file='%1'").arg(proc->logFilename());

    //FIXME:
    args << "--no-encryption";
/*
    switch (jobType) {
    case Job::Type::BackupAuto:
    case Job::Type::BackupFull:
    case Job::Type::BackupIncremental:
        args << pathWin2Cygwin(backup->path).toUtf8()
              << "file://"+pathWin2Cygwin(storage->path).toUtf8()
                 + (!storage->path.endsWith("/") && !job->subdir.startsWith("/") ? "/" : "" ) + job->subdir;
        break;
    case Job::Type::Verify:
        args << "file://"+pathWin2Cygwin(storage->path)
                + (!storage->path.endsWith("/") && !job->subdir.startsWith("/") ? "/" : "" ) + job->subdir
             << pathWin2Cygwin(backup->path);
        break;
    case Job::Type::ListCurrentFiles:
        args << "file://"+pathWin2Cygwin(storage->path)
                + (!storage->path.endsWith("/") && !job->subdir.startsWith("/") ? "/" : "" ) + job->subdir;
        break;
    case Job::Type::CollectionStatus:
        qWarning() << Q_FUNC_INFO << ':' << __LINE__ << "CollectionStatus not implemented!";
    }

*/

    //DEBUG
    qDebug("Arguments:");
    for(const auto &arg : args)
        qDebug("%s", qPrintable(arg));

    return args;
}


bool QDuplyLib::runJob(const QString &jobName, const Job::Type &jobType)
{
    /*if(!_mapJobs.contains(jobName)) {
        std::cerr << "Job '" << qPrintable(jobName) << "' not found" << std::endl;
        return false;
    }

    JobPtr job = _mapJobs.value(jobName);

    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    env.insert("PASSPHRASE", job->options.value(O_GpgPassword, "").toString());
    env.insert("SIGN_PASSPHRASE", job->options.value(O_GpgSignPassword, "").toString());

    DuplicityProcess *proc = new DuplicityProcess;
    proc->jobName = jobName;

    //TODO: in lambda sender() == 0; need to find fix
    connect(proc, &QProcess::readyReadStandardOutput, this, &QDuplyLib::onJobReadyReadStdout);
    connect(proc, &QProcess::readyReadStandardError, this, &QDuplyLib::onJobReadyReadStderr);
    connect(proc, static_cast<void (QProcess:: *)(QProcess::ProcessError)>(&QProcess::error), this, &QDuplyLib::onJobError);
    connect(proc, static_cast<void (QProcess:: *)(int, QProcess::ExitStatus)>(&QProcess::finished), this, &QDuplyLib::onJobFinished);

    //gen cmd
    QStringList args = generateArgsJob(jobName, jobType, proc);
    proc->setProcessEnvironment(env);


    QString cmd = DUPLICITY_CMD + " " + args.join(' ');
    qDebug("%s", qPrintable(cmd));

    proc->start(DUPLICITY_CMD, args);
*/
    return true;
}


bool QDuplyLib::runVerify(const QString &jobName)
{
    /*
    if(!_mapJobs.contains(jobName)) {
        std::cerr << "Job '" << qPrintable(jobName) << "' not found" << std::endl;
        return false;
    }

    JobPtr job = _mapJobs.value(jobName);

    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    env.insert("PASSPHRASE", job->options.value(O_GpgPassword, "").toString());
    env.insert("SIGN_PASSPHRASE", job->options.value(O_GpgSignPassword, "").toString());

    DuplicityProcess *proc = new DuplicityProcess;
    proc->jobName = jobName;
    
    connect(proc, &QProcess::readyReadStandardOutput, this, &QDuplyLib::onJobReadyReadStdout);
    connect(proc, &QProcess::readyReadStandardError, this, &QDuplyLib::onJobReadyReadStderr);
    connect(proc, static_cast<void (QProcess:: *)(QProcess::ProcessError)>(&QProcess::error), this, &QDuplyLib::onJobError);
    connect(proc, static_cast<void (QProcess:: *)(int, QProcess::ExitStatus)>(&QProcess::finished), this, &QDuplyLib::onJobFinished);

    QStringList args = generateArgsJob(jobName, Job::Type::Verify, proc);
    proc->setProcessEnvironment(env);

    QString cmd = DUPLICITY_CMD + " " + args.join(' ');
    qDebug("%s", qPrintable(cmd));

    proc->start(DUPLICITY_CMD, args);
    */
    return true;
}


bool QDuplyLib::runListCurrentFiles(const QString &jobName)
{
    /*
    if(!_mapJobs.contains(jobName)) {
        std::cerr << "Job '" << qPrintable(jobName) << "' not found" << std::endl;
        return false;
    }

    JobPtr job = _mapJobs.value(jobName);
    BackupPtr backup = _mapBackups.value(job->backup);
    StoragePtr storage = _mapStorages.value(job->storage);

    auto env = QProcessEnvironment::systemEnvironment();
    env.insert("PASSPHRASE", job->options.value(O_GpgPassword, "").toString());
    env.insert("SIGN_PASSPHRASE", job->options.value(O_GpgSignPassword, "").toString());

    auto proc = new DuplicityProcess;
    proc->jobName = jobName;

    connect(proc, &QProcess::readyReadStandardOutput, this, &QDuplyLib::onJobReadyReadStdout);
    connect(proc, &QProcess::readyReadStandardError, this, &QDuplyLib::onJobReadyReadStderr);
    connect(proc, static_cast<void (QProcess:: *)(QProcess::ProcessError)>(&QProcess::error), this, &QDuplyLib::onJobError);
    connect(proc, static_cast<void (QProcess:: *)(int, QProcess::ExitStatus)>(&QProcess::finished), this, &QDuplyLib::onJobFinished);

    auto args = generateArgsJob(jobName, Job::Type::ListCurrentFiles, proc);

    //generate cmd
    QString cmd = DUPLICITY_CMD + " "+ args.join(' ');
    qDebug("%s", qPrintable(cmd));

    proc->setProcessEnvironment(env);
    proc->start(DUPLICITY_CMD, args);
    */
    return true;
}


void QDuplyLib::onJobReadyReadStdout()
{
    DuplicityProcess *proc = static_cast<DuplicityProcess*>(sender());
   // emit sigStdOutJob();
}


void QDuplyLib::onJobReadyReadStderr()
{
    DuplicityProcess *proc = static_cast<DuplicityProcess*>(sender());
   // emit sigStdErrJob(proc->job, proc->readAllStandardError());
}


void QDuplyLib::onJobError(const QProcess::ProcessError &)
{
    QProcess *proc = static_cast<QProcess*>(sender());
    std::cerr << proc->errorString().toStdString() << std::flush;
}


void QDuplyLib::onJobFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
    DuplicityProcess *proc = static_cast<DuplicityProcess*>(sender());
    std::cout << " Job '" << proc->job.name
              << "' finished with exit code " << exitCode << " (status " << exitStatus << ')' << std::endl;

    proc->deleteLater();
}


void QDuplyLib::listCurrentFilesStdOutput()
{
    QProcess *proc = static_cast<QProcess*>(sender());
    std::cout << proc->readAllStandardOutput().constData() << std::flush;
}

void QDuplyLib::listCurrentFilesStdError()
{
    QProcess *proc = static_cast<QProcess*>(sender());
    std::cerr << proc->readAllStandardError().constData() << std::flush;
}


QString QDuplyLib::pathWin2Cygwin(const QString &winPath)
{
    //escape
 /*   QString path = winPath;
    path.replace(" ", "\\ ");
    qDebug("%s", qPrintable(path));

    //check if path is Windows-style
    QRegularExpression re("^[a-zA-Z]:.*");
    QRegularExpressionMatch match = re.match(path);
    if(!match.hasMatch())
        return path;
*/
    //take first letter - disk drive
    QChar disk = winPath.at(0);
    QString cygwinPath = winPath;
    cygwinPath.replace(0, 2, QLatin1String("/cygdrive/")+disk.toLower()); // replace 'X:' to '/cygdrive/x'
    return cygwinPath;
}


bool QDuplyLib::write()
{
    QDuplyConfig conf;
    conf.backups  = backups_;
    conf.storages = storages_;
    conf.jobs     = jobs_;
    return conf.write(configPath_);
}
