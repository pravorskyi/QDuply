/*
    Copyright © Andrii Pravorskyi 2017

    This file is part of QDuply.

    QDuply is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    QDuply is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QDuply.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include "structs.h"


class Job
{
public:
    //TODO: [REFACTORING] move values to namespace "Type"
    // or rename to TypeFoo, TypeBar, etc.
    // We need consistently naming convention.
    enum Type {
        BackupAuto,
        BackupFull,
        BackupIncremental,
        CollectionStatus,
        ListCurrentFiles,
        Verify
    };

    explicit Job(BackupPtr & backup, StoragePtr & storage);

    void setGlobalDuplicityOptions(const DuplicityOptionsPtr & options);
    void setGlobalGpgOptions(const GpgOptionsPtr & options);

    std::string         name;
    std::string         subdir;
    BackupPtr           backup;
    StoragePtr          storage;
    DuplicityOptionsPtr duplicity_options;
    GpgOptionsPtr       gpg_options;

    static const std::string& typeStr(const Type & type);
};

using JobPtr  = std::shared_ptr<Job>;
using Jobs    = std::vector<JobPtr>;
using JobsPtr = std::shared_ptr<Jobs>;
