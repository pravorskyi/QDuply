/*
    Copyright © Andrii Pravorskyi 2017

    This file is part of QDuply.

    QDuply is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    QDuply is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QDuply.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include <QtCore/QVariant>
#include <map>
#include <memory>
#include <set>
#include <string>

//TODO: replace QVariant with std::any [C++17] and remove Q_DECLARE_METATYPE macros
//TODO: delete after migration to std::any [C++17]
Q_DECLARE_METATYPE (char*)
Q_DECLARE_METATYPE (signed long)

using DuplicityOptions    = std::map<std::string, QVariant>;
using DuplicityOptionsPtr = std::shared_ptr<DuplicityOptions>;

using GpgOptions          = std::map<std::string, QVariant>;
using GpgOptionsPtr       = std::shared_ptr<GpgOptions>;


const std::set<std::string> & AvailableDuplicityOptions();
const std::set<std::string> & AvailableGpgOptions();


struct Backup {
    std::string         name;
    std::string         path;
    DuplicityOptionsPtr duplicity_options;
    GpgOptionsPtr       gpg_options;
};

using BackupPtr  = std::shared_ptr<Backup>;
using Backups    = std::vector<BackupPtr>;
using BackupsPtr = std::shared_ptr<Backups>;


struct Storage {
    std::string         name;
    std::string         path;
    DuplicityOptionsPtr duplicity_options;
    GpgOptionsPtr       gpg_options;
};

using StoragePtr  = std::shared_ptr<Storage>;
using Storages    = std::vector<StoragePtr>;
using StoragesPtr = std::shared_ptr<Storages>;

//TODO: delete after migration to std::any [C++17]
Q_DECLARE_METATYPE(std::vector<std::string>)
