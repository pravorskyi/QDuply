project(QDuplyLib CXX)

find_package(Qt5Core REQUIRED)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)

set(HEADERS
    duplicityprocess.h
    job.h
    structs.h
    qduplyconfig.h
    qduplylib.h
)

set(SOURCES
    duplicityprocess.cpp
    job.cpp
    structs.cpp
    qduplyconfig.cpp
    qduplylib.cpp
)


add_library(${PROJECT_NAME} SHARED ${HEADERS} ${SOURCES})
qt5_use_modules(${PROJECT_NAME} Core)
target_link_libraries(${PROJECT_NAME} confuse)

install(TARGETS ${PROJECT_NAME} DESTINATION lib)
