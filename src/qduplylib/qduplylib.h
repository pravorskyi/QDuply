/*
    Copyright © Andrii Pravorskyi 2017

    This file is part of QDuply.

    QDuply is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    QDuply is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QDuply.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include <QtCore/QCoreApplication>
#include <QtCore/QMap>
#include <QtCore/QObject>
#include <QtCore/QProcess>
#include "job.h"
#include "structs.h"
#include "qduplyconfig.h"


class DuplicityProcess;

class QDuplyLib : public QObject
{
    Q_OBJECT
public:
    QDuplyLib(QObject * parent = 0);

    std::string errorMessage() const { return error_; }

    void setConfigPath(const std::string &path);

    std::string        configPath() const { return configPath_; }
    inline BackupsPtr  backups()    const { return backups_; }
    inline JobsPtr     jobs()       const { return jobs_; }
    inline StoragesPtr storages()   const { return storages_; }

    bool write();

    bool runCollectionStatus(const QString &jobName);
    bool runJob(const QString &jobName, const Job::Type &jobType);
    bool runVerify(const QString &JobName);
    bool runListCurrentFiles(const QString &jobName);

    // convert filesystem path from windows-style to cygwin-style;
    // example: "C:/devel" -> "/cygdrive/c/devel"
    static QString pathWin2Cygwin(const QString &winPath);

private:
    std::string configPath_;
    std::string error_;

    BackupsPtr  backups_;
    StoragesPtr storages_;
    JobsPtr     jobs_;

    QMap<QString, std::shared_ptr<QProcess*> > _currentJobs;

    QStringList generateArgsJob(const JobPtr &job, const Job::Type &jobType, DuplicityProcess* const) const;

    void listCurrentFilesStdOutput();
    void listCurrentFilesStdError();

    void onJobReadyReadStdout();
    void onJobReadyReadStderr();
    void onJobError(const QProcess::ProcessError &err);
    void onJobFinished(int exitCode, QProcess::ExitStatus exitStatus);

    QString DUPLICITY_CMD = QString("duplicity");

public slots:
    bool init();


signals:
    void sigInitFinished(bool ok);
    void sigStdOutJob();
    void sigStdErrJob();
};
