/*
    Copyright © Andrii Pravorskyi 2017

    This file is part of QDuply.

    QDuply is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    QDuply is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QDuply.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "corecli.h"
#include "../qduplylib/qduplylib.h"

#include <QtCore/QDir>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>

CoreCli::CoreCli(): QObject()
{
    _corelib = new QDuplyLib(this);
}


bool CoreCli::init(int argc, char *argv[])
{
    _corelib->setConfigPath(QDir::homePath() + "/test.conf");
    if(!_corelib->init())
        return false;
    
    _argc = argc;
    _argv = argv;

    return true;
}


void CoreCli::run()
{
    parseOptions(_argc, _argv);
}


void CoreCli::parseOptions(int argc, char *argv[])
{
    namespace po = boost::program_options;
    po::options_description desc("General options");
    std::string job_name;
    std::string type_backup;
    desc.add_options()
        ("help,h", "Show help")
        ("job,j", po::value<std::string>(&job_name), "Run job <name>")
        ("type,t", po::value<std::string>(&type_backup), "Type of backup: full, increment or auto");
        
    
    po::options_description train_desc("Train options");
    train_desc.add_options()
        ("input,I", po::value<std::string>(), "Input .dat file")
        ("info,i", po::value<std::string>(), "Input .trn file")
        ("output,O", po::value<std::string>(), "Output parameters file .prs");

    po::options_description recognize_desc("Recognize options");
    recognize_desc.add_options()
        ("input,I",  po::value<std::vector<std::string> >(), "Input .dat file")
        ("params,p", po::value<std::string>(), "Input .prs file")
        ("output,O", po::value<std::string>(), "Output directory");
        
    po::options_description score_desc("Score options");
    score_desc.add_options()
        ("ethanol,e",  po::value<std::string>(), "Etalon .trn file")
        ("test,t", po::value<std::string>(), "Testing .trn file")
        ("output,O", po::value<std::string>(), "Output comparison file");

    
    
    po::variables_map vm;
    po::parsed_options parsed = po::command_line_parser(argc, argv).options(desc).allow_unregistered().run();
    po::store(parsed, vm);
    po::notify(vm);

    
    if (vm.count("help")) {
        std::cout << desc << "\n";
        exit(EXIT_SUCCESS);
    }
    
    if(vm.count("job")) {
        desc.add(train_desc);
        po::store(po::parse_command_line(argc,argv,desc), vm);
        if(!_corelib->runJob(QString::fromStdString(job_name), JobType::Auto)) {
            exit(EXIT_FAILURE);
        }

        connect(_corelib, &QDuplyLib::sigStdOutJob, [=] (const QString &, const QByteArray &text) {
            std::cout << text.constData();
        });
        
        connect(_corelib, &QDuplyLib::sigStdErrJob, [=] (const QString &, const QByteArray &text) {
            std::cerr << text.constData() << std::flush;
        });
    }
  
}
