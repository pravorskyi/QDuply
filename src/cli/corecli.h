/*
    Copyright © Andrii Pravorskyi 2017

    This file is part of QDuply.

    QDuply is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    QDuply is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QDuply.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CORECLI_H
#define CORECLI_H

#include <QtCore/QObject>

class QDuplyLib;

class CoreCli: public QObject
{
    Q_OBJECT
public:
    CoreCli();
    bool init(int argc, char *argv[]);

public slots:
    void run();

private:
    QDuplyLib* _corelib;
    int _argc;
    char **_argv;

    void parseOptions(int argc, char *argv[]);
};

#endif // CORECLI_H
