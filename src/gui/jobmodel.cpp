/*
    Copyright © Andrii Pravorskyi 2017

    This file is part of QDuply.

    QDuply is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    QDuply is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QDuply.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "jobmodel.h"

JobModel::JobModel(const JobsPtr &jobs, QObject* parent):
    QAbstractTableModel(parent)
{
    jobs_ = jobs;
}


int JobModel::columnCount(const QModelIndex &) const
{
    return 3;
}


int JobModel::rowCount(const QModelIndex &) const
{
    return jobs_->size();
}


QVariant JobModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role != Qt::DisplayRole || orientation != Qt::Horizontal)
        return QAbstractItemModel::headerData(section, orientation, role);

    switch(section) {
        case 0: return tr("Name");
        case 1: return tr("Backup");
        case 2: return tr("Storage");
    }

    return QAbstractItemModel::headerData(section, orientation, role);
}


QVariant JobModel::data(const QModelIndex & index, int role) const
{
    if(rowCount() == 0)
        return QVariant();

    auto& j = jobs_->at(index.row());

    switch(role) {
        case Qt::DisplayRole:
            switch(index.column()) {
            case 0: return QString::fromStdString(j->name);
            case 1: return QString::fromStdString(j->backup->name);
            case 2: return QString::fromStdString(j->storage->name);
            }

        case RoleName:
            return QString::fromStdString(j->name);
    }

    return QVariant();
}


bool JobModel::append(const JobPtr &job)
{
    if (std::any_of(jobs_->cbegin(), jobs_->cend(), [&job] (const JobPtr & p) { return p->name == job->name; }))
        return false;

    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    jobs_->push_back(job);
    endInsertRows();

    return true;
}
