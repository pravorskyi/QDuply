/*
    Copyright © Andrii Pravorskyi 2017

    This file is part of QDuply.

    QDuply is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    QDuply is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QDuply.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include <QtWidgets/QMainWindow>
#include <QtCore/QMap>
#include <QtCore/QProcess>

struct Backup;
struct Job;
struct Storage;

namespace Ui {
class MainWindow;
}

class BackupModel;
class JobModel;
class StorageModel;
class QDuplyLib;

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
    bool init();
    
    void readProfiles();
    
private:
    Ui::MainWindow * ui;
    QDuplyLib *      _core          = nullptr;
    BackupModel *    modelBackups_  = nullptr;
    JobModel *       modelJobs_     = nullptr;
    StorageModel *   modelStorages_ = nullptr;

    void report();
    void processError(QProcess::ProcessError error);
    void processFinished(int exitCode, QProcess::ExitStatus exitStatus);

    void verify();
    
    void jobRun();
    void listCurrentFiles();
    void onBackupAdd();
    void onCoreInitFinished(bool ok);
    void onJobAdd();
    void onJobDebugClear();
    void onSaveConfig();
    void onSaveConfigAs();
    void onSourceSelected();
    void onStorageAdd();
    void saveConfig(const std::string &filename);
private slots:
    void onJobStdOut(QString, QByteArray);
    void onJobStdErr(QString, QByteArray);
};
