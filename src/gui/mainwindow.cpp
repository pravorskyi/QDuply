/*
    Copyright © Andrii Pravorskyi 2017

    This file is part of QDuply.

    QDuply is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    QDuply is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QDuply.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "backupmodel.h"
#include "jobmodel.h"
#include "storagemodel.h"
#include "ui/dlgjob.h"
#include "ui/dlgstorage.h"
#include "ui/dlgbackup.h"
#include "../qduplylib/qduplylib.h"

#include <QtCore/QStandardPaths>
#include <QtCore/QDebug>
#include <QtCore/QPointer>
#include <QtCore/QTemporaryDir>
#include <QtCore/QThread>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QMessageBox>
#include <iostream>

static const int COLUMN_JOB_NAME = 0;
static const int COLUMN_JOB_BACKUP = 1;
static const int COLUMN_JOB_STORAGE = 2;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->tblBackups->horizontalHeader()->setStretchLastSection(true);
    ui->tblBackups->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    ui->tblBackups->setSelectionBehavior(QAbstractItemView::SelectRows);

    ui->tblStorages->horizontalHeader()->setStretchLastSection(true);
    ui->tblStorages->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    ui->tblStorages->setSelectionBehavior(QAbstractItemView::SelectRows);

    ui->tblJobs->horizontalHeader()->setStretchLastSection(true);
    ui->tblJobs->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    ui->tblJobs->setSelectionBehavior(QAbstractItemView::SelectRows);

    connect(ui->actionSave, &QAction::triggered, this, &MainWindow::onSaveConfig);
    connect(ui->actionSave_as, &QAction::triggered, this, &MainWindow::onSaveConfigAs);
    connect(ui->listWidget, &QListWidget::currentRowChanged, ui->stackedWidget, &QStackedWidget::setCurrentIndex);


    connect(ui->btnBackupAdd, &QPushButton::clicked, this, &MainWindow::onBackupAdd);

    // storage page
    connect(ui->btnStorageAdd, &QPushButton::clicked, this, &MainWindow::onStorageAdd);

    // jobs page
    connect(ui->btnJobAdd, &QPushButton::clicked, this, &MainWindow::onJobAdd);
    connect(ui->btnJobRun, &QPushButton::clicked, this, &MainWindow::jobRun);
    connect(ui->btnJobDebugClear, &QPushButton::clicked, this, &MainWindow::onJobDebugClear);
    connect(ui->btnJobVerify, &QPushButton::clicked, this, &MainWindow::verify);
    connect(ui->btnReportAll, &QPushButton::clicked, this, &MainWindow::report);
    connect(ui->btnJobListCurrentFiles, &QPushButton::clicked, this, &MainWindow::listCurrentFiles);

    _core = new QDuplyLib;
    auto coreThread = new QThread(this);
    _core->moveToThread(coreThread);
    connect(_core, &QDuplyLib::sigInitFinished, this, &MainWindow::onCoreInitFinished);
    connect(_core, SIGNAL(sigStdErrJob(QString,QByteArray)), this, SLOT(onJobStdErr(QString,QByteArray)));
    connect(_core, SIGNAL(sigStdOutJob(QString,QByteArray)), this, SLOT(onJobStdOut(QString,QByteArray)));
}

MainWindow::~MainWindow()
{
    _core->thread()->quit();
    delete ui;
    _core->thread()->wait();
}


bool MainWindow::init()
{
    _core->thread()->start();
    _core->setConfigPath(QDir::homePath().toStdString() + "/test.conf");

    QMetaObject::invokeMethod(_core, "init", Qt::QueuedConnection);
    return true;
}


void MainWindow::onCoreInitFinished(bool ok)
{
    if(ok) {
        ui->stackedWidget->setEnabled(true);
        ui->listWidget->setEnabled(true);
        ui->actionSave_as->setEnabled(true);
        readProfiles();
    } else {
        QMessageBox::critical(this, "Read config error", QString::fromStdString(_core->errorMessage()));
    }
}


void MainWindow::readProfiles()
{
    modelBackups_ = new BackupModel(_core->backups(), this);
    ui->tblBackups->setModel(modelBackups_);
    
    modelStorages_ = new StorageModel(_core->storages(), this);
    ui->tblStorages->setModel(modelStorages_);
    
    modelJobs_ = new JobModel(_core->jobs(), this);
    ui->tblJobs->setModel(modelJobs_);
}


void MainWindow::report()
{
    /*
    ui->txtReport->clear();
    QTemporaryDir *dir = new QTemporaryDir;
    if (!dir->isValid()) {
        qWarning("%s:%d Can't create temporary dir.", Q_FUNC_INFO, __LINE__);
        ui->txtReport->append("Can't create temporary dir.");
        return;
    }
    for(const Job &job : m_Jobs) {
        Storage storage = m_Storages.value(job.storage);
        QString cmd = QString("duplicity collection-status --log-file %1/%2 %3%4")
                .arg(dir->path(), job.name, storage.path, job.subdir);
        qDebug("%s", qPrintable(cmd));

        QProcess *proc = new QProcess(this);
        //FIXME: delete later
        connect(proc, &QProcess::readyReadStandardOutput, this, &MainWindow::readyReadStandartOutput);
        connect(proc, &QProcess::readyReadStandardError, this, &MainWindow::readyReadStandartError);
        connect(proc, static_cast<void (QProcess:: *)(int, QProcess::ExitStatus)>(&QProcess::finished), this, &MainWindow::processFinished);
        proc->start(cmd);
    }
    */
}



void MainWindow::processError(QProcess::ProcessError error)
{
    qDebug("Process error: %d", error);
}


void MainWindow::processFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
    QProcess *proc = static_cast<QProcess*>(sender());
    qDebug("FINISHED! exit code %d", exitCode);
    proc->deleteLater();
}


void MainWindow::verify()
{
    QString jobName = ui->tblJobs->model()->data(ui->tblJobs->currentIndex(), JobModel::RoleName).toString();
    if(jobName.isEmpty())
        return;

    if(!_core->runVerify(jobName)) {
        exit(EXIT_FAILURE);
    }
}

void MainWindow::jobRun()
{
    QString jobName = ui->tblJobs->model()->data(ui->tblJobs->currentIndex(), JobModel::RoleName).toString();
    if(jobName.isEmpty())
        return;

    //_core->jobRun(jobName, ui->pgJobs->rbtn);
    Job::Type jobType = Job::Type::BackupAuto;
    if(ui->rbtnJobFull->isChecked())
        jobType = Job::Type::BackupFull;
    else if(ui->rbtnJobIncremental->isChecked())
        jobType = Job::Type::BackupIncremental;

    if(!_core->runJob(jobName, jobType)) {
        exit(EXIT_FAILURE);
    }
 /*   connect(_core, &QDuplyLib::sigStdOutJob, [=] (const QString &, const QByteArray &text) {
                ui->txtJobLog->append(text);
                qApp->processEvents();
                std::cout << text.constData() << std::flush;
            });
            */

}


void MainWindow::onJobAdd()
{
    /*
    QPointer<DlgJob> dlg = new DlgJob;

    QStringList targets;
    for(const BackupPtr &backup : *_core->backups().get())
        targets << backup->name;
    dlg->setTargets(targets);

    QStringList storages;
    for(const StoragePtr &storage : *_core->storages().get())
        storages << storage->name;
    dlg->setStorages(storages);

    if(dlg->exec() == QDialog::Accepted) {
        // add target
        JobPtr job(new Job);
        job->name = dlg->name();
        job->backup = dlg->target();
        job->storage = dlg->storage();
        job->subdir = dlg->subdir();

        if(_core->jobAdd(job))
            _modelJobs->append(job);
        else
            QMessageBox::warning(this, qApp->applicationName(),
                tr("Job %1 already exists.").arg(job->name), QMessageBox::Ok);

    }

    delete dlg;
    */
}


void MainWindow::onJobDebugClear()
{
    ui->txtJobLog->clear();
}


void MainWindow::onSaveConfig()
{
    saveConfig(_core->configPath());
}


void MainWindow::onSaveConfigAs()
{
    QString filename = QFileDialog::getSaveFileName(this, "Save file", "", ".conf");
    if(!filename.isEmpty())
        saveConfig(filename.toStdString());
}


void MainWindow::onSourceSelected()
{

}


void MainWindow::onStorageAdd()
{
    QPointer<DlgStorage> dlg = new DlgStorage;
    if(dlg->exec() == QDialog::Accepted) {
        auto storage = std::make_shared<Storage>();
        storage->name = dlg->name().toStdString();
        storage->path = dlg->path().toStdString();

        if(!modelStorages_->append(storage))
            QMessageBox::warning(this, qApp->applicationName(),
                tr("Storage %1 already exists.").arg(storage->name.c_str()), QMessageBox::Ok);
    }

    delete dlg;
}


void MainWindow::onBackupAdd()
{
    QPointer<DlgBackup> dlg = new DlgBackup;
    if(dlg->exec() == QDialog::Accepted) {
        auto backup = std::make_shared<Backup>();
        backup->name = dlg->name().toStdString();
        backup->path = dlg->path().toStdString();

        if(!modelBackups_->append(backup))
            QMessageBox::warning(this, qApp->applicationName(),
                tr("Backup %1 already exists.").arg(backup->name.c_str()), QMessageBox::Ok);
    }

    delete dlg;
}


void MainWindow::saveConfig(const std::string &filename)
{
    _core->setConfigPath(filename);
    _core->write();
}


void MainWindow::onJobStdOut(QString, QByteArray text)
{
    ui->txtJobLog->insertPlainText(text);
    std::cout << text.constData() << std::flush;
}


void MainWindow::onJobStdErr(QString, QByteArray text)
{
    ui->txtJobLog->insertPlainText(text);
    std::cerr << text.constData() << std::flush;
}


void MainWindow::listCurrentFiles()
{
    QString jobName = ui->tblJobs->model()->data(ui->tblJobs->currentIndex(), JobModel::RoleName).toString();
    if(!_core->runListCurrentFiles(jobName)) {
        exit(EXIT_FAILURE);
    }
}
