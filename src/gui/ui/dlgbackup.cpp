#include "dlgbackup.h"
#include "ui_dlgbackup.h"
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QMessageBox>


DlgBackup::DlgBackup(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DlgBackup)
{
    ui->setupUi(this);
    connect(ui->pushButton, &QPushButton::clicked,       this, &DlgBackup::onBrowse);
    connect(ui->buttonBox,  &QDialogButtonBox::accepted, this, &DlgBackup::onAccept);
}

DlgBackup::~DlgBackup()
{
    delete ui;
}


void DlgBackup::onBrowse()
{
    QString directory = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
        "", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

    if(!directory.isEmpty())
        ui->edtPath->setText(directory);
}


void DlgBackup::onAccept()
{
    if(ui->edtName->text().isEmpty()) {
        QMessageBox::warning(this, qApp->applicationName(), tr("Target name can't be empty."), QMessageBox::Ok);
        return;
    }

    if(ui->edtPath->text().isEmpty()) {
        QMessageBox::warning(this, qApp->applicationName(), tr("Path can't be empty."), QMessageBox::Ok);
        return;
    }

    QDir dir(ui->edtPath->text());
    if(!dir.exists()) {
        QMessageBox::warning(this, qApp->applicationName(), tr("Directory not exists."), QMessageBox::Ok);
        return;
    }

    accept();
}


QString DlgBackup::name() const
{
    return ui->edtName->text();
}


QString DlgBackup::path() const
{
    return ui->edtPath->text();
}
