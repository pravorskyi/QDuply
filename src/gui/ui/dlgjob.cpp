#include "dlgjob.h"
#include "ui_dlgjob.h"

DlgJob::DlgJob(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DlgJob)
{
    ui->setupUi(this);
}

DlgJob::~DlgJob()
{
    delete ui;
}


void DlgJob::setStorages(const QStringList &list)
{
    ui->bxStorages->insertItems(0, list);
}


void DlgJob::setTargets(const QStringList &list)
{
    ui->bxTargets->insertItems(0, list);
}


QString DlgJob::name() const
{
    return ui->edtName->text();
}


QString DlgJob::storage() const
{
    return ui->bxStorages->currentText();
}


QString DlgJob::subdir() const
{
    return ui->edtSubdir->text();
}


QString DlgJob::target() const
{
    return ui->bxTargets->currentText();
}
