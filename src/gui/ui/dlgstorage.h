#pragma once
#include <QDialog>

namespace Ui {
class DlgStorage;
}

class DlgStorage : public QDialog
{
    Q_OBJECT

public:
    explicit DlgStorage(QWidget *parent = 0);
    ~DlgStorage();

    QString name() const;
    QString path() const;

private:
    Ui::DlgStorage *ui;

    void onBrowse();
    void onAccept();
};
