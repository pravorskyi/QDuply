#pragma once
#include <QDialog>

namespace Ui {
class DlgJob;
}

class DlgJob : public QDialog
{
    Q_OBJECT

public:
    explicit DlgJob(QWidget *parent = 0);
    ~DlgJob();

    QString name() const;
    QString storage() const;
    QString subdir() const;
    QString target() const;

    void setStorages(const QStringList &list);
    void setTargets(const QStringList &list);


private:
    Ui::DlgJob *ui;
};
