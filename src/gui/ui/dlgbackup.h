#pragma once
#include <QtWidgets/QDialog>

namespace Ui {
class DlgBackup;
}

class DlgBackup : public QDialog
{
    Q_OBJECT

public:
    explicit DlgBackup(QWidget *parent = 0);
    ~DlgBackup();

    QString name() const;
    QString path() const;
private:
    Ui::DlgBackup *ui;

    void onBrowse();
    void onAccept();
};
