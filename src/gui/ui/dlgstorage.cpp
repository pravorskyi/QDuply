#include "dlgstorage.h"
#include "ui_dlgstorage.h"

#include <QtWidgets/QFileDialog>
#include <QtWidgets/QMessageBox>

DlgStorage::DlgStorage(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DlgStorage)
{
    ui->setupUi(this);
    connect(ui->pushButton, &QPushButton::clicked, this, &DlgStorage::onBrowse);
    connect(ui->buttonBox, &QDialogButtonBox::accepted, this, &DlgStorage::onAccept);
}

DlgStorage::~DlgStorage()
{
    delete ui;
}

void DlgStorage::onBrowse()
{
    QString directory = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
        "", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

    if(!directory.isEmpty())
        ui->edtPath->setText(directory);
}


void DlgStorage::onAccept()
{
    if(ui->edtName->text().isEmpty()) {
        QMessageBox::warning(this, qApp->applicationName(), tr("Target name can't be empty."), QMessageBox::Ok);
        return;
    }

    if(ui->edtPath->text().isEmpty()) {
        QMessageBox::warning(this, qApp->applicationName(), tr("Path can't be empty."), QMessageBox::Ok);
        return;
    }

    QDir dir(ui->edtPath->text());
    if(!dir.exists()) {
        QMessageBox::warning(this, qApp->applicationName(), tr("Directory not exists."), QMessageBox::Ok);
        return;
    }

    accept();
}


QString DlgStorage::name() const
{
    return ui->edtName->text();
}


QString DlgStorage::path() const
{
    return ui->edtPath->text();
}
