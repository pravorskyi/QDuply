/*
    Copyright © Andrii Pravorskyi 2017

    This file is part of QDuply.

    QDuply is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    QDuply is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QDuply.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include <QtCore/QAbstractTableModel>
#include "../qduplylib/structs.h"
#include "../qduplylib/job.h"

class JobModel: public QAbstractTableModel
{
public:
    enum ItemDataRole {
        RoleName = 0x0100
    };
    
    explicit JobModel(const JobsPtr &storages, QObject* parent = 0);
    int columnCount(const QModelIndex & parent = QModelIndex()) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const override;

    //
    bool append(const JobPtr &job);
    
private:
    JobsPtr jobs_;
};
