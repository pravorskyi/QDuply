/*
    Copyright © Andrii Pravorskyi 2017

    This file is part of QDuply.

    QDuply is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    QDuply is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QDuply.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mainwindow.h"
#include <QtWidgets/QApplication>


int main(int argc, char *argv[])
{
    QApplication::setApplicationName("QDuply");
    QApplication a(argc, argv);

    MainWindow w;
    if(!w.init())
        return EXIT_FAILURE;

    w.show();

    return a.exec();
}
