/*
    Copyright © Andrii Pravorskyi 2017

    This file is part of QDuply.

    QDuply is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    QDuply is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QDuply.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "backupmodel.h"

BackupModel::BackupModel(const BackupsPtr &backups, QObject* parent):
    QAbstractTableModel(parent)
{
    backups_ = backups;
}


int BackupModel::columnCount(const QModelIndex &) const
{
    return 2;
}


int BackupModel::rowCount(const QModelIndex &) const
{
    return backups_->size();
}


QVariant BackupModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role != Qt::DisplayRole || orientation != Qt::Horizontal)
        return QAbstractItemModel::headerData(section, orientation, role);
    
    switch(section) {
        case 0: return tr("Name");
        case 1: return tr("Path");
    }
        
    return QAbstractItemModel::headerData(section, orientation, role);
}


QVariant BackupModel::data(const QModelIndex & index, int role) const
{
    if(role != Qt::DisplayRole)
        return QVariant();
        
    auto& b = backups_->at(index.row());
    
    switch(index.column()) {
    case 0: return QString::fromStdString(b->name);
    case 1: return QString::fromStdString(b->path);
    }
    
    return QVariant();
}


bool BackupModel::append(const BackupPtr &backup)
{
    if (std::any_of(backups_->cbegin(), backups_->cend(), [&backup] (const BackupPtr & p) { return p->name == backup->name; }))
        return false;

    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    backups_->push_back(backup);
    endInsertRows();

    return true;
}
