/*
    Copyright © Andrii Pravorskyi 2017

    This file is part of QDuply.

    QDuply is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    QDuply is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QDuply.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "storagemodel.h"

StorageModel::StorageModel(const StoragesPtr &storages, QObject* parent):
    QAbstractTableModel(parent)
{
    storages_ = storages;
}


int StorageModel::columnCount(const QModelIndex &) const
{
    return 2;
}


int StorageModel::rowCount(const QModelIndex &) const
{
    return storages_->size();
}


QVariant StorageModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role != Qt::DisplayRole || orientation != Qt::Horizontal)
        return QAbstractItemModel::headerData(section, orientation, role);
    
    switch(section) {
        case 0: return tr("Name");
        case 1: return tr("Path");
    }
        
    return QAbstractItemModel::headerData(section, orientation, role);
}


QVariant StorageModel::data(const QModelIndex & index, int role) const
{
    if(role != Qt::DisplayRole)
        return QVariant();
        
    auto& s = storages_->at(index.row());
    
    switch(index.column()) {
    case 0: return QString::fromStdString(s->name);
    case 1: return QString::fromStdString(s->path);
    }
    
    return QVariant();
}


bool StorageModel::append(const StoragePtr &storage)
{
    if (std::any_of(storages_->cbegin(), storages_->cend(),
                    [&storage] (const StoragePtr & p) { return p->name == storage->name; }))
       return false;

    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    storages_->push_back(storage);
    endInsertRows();

    return true;
}
