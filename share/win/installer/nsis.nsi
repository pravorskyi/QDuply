RequestExecutionLevel admin

!include MUI.nsh

Name "QDuply"

# name the installer
OutFile "QDuply_setup.exe"
InstallDir "$PROGRAMFILES\QDuply"


LoadLanguageFile "${NSISDIR}\Contrib\Language files\Ukrainian.nlf"

SetCompress off
#SetCompress force

#SetCompressor /SOLID lzma

!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
#!insertmacro MUI_LANGUAGE "English"
#!insertmacro MUI_PAGE_FINISH


# default section start; every NSIS script has at least one section.
section

    SetOutPath $INSTDIR
    File  /r "qduply_install\"
    File  "qduply-build\src\gui\QDuplyGUI.exe"
    File  "qduply-build\src\qduplylib\libQDuplyLib.dll"

    writeUninstaller "$INSTDIR\uninstall.exe"
    
    #start menu
    createDirectory "$SMPROGRAMS\QDuply"
    createShortCut "$SMPROGRAMS\QDuply\QDuply.lnk" "$INSTDIR\QDuplyGUI.exe"
    createShortCut "$SMPROGRAMS\QDuply\Uninstall.lnk" "$INSTDIR\uninstall.exe"

    SetOutPath "$INSTDIR\cygwin"
    File /a /r "cygwin_duplicity\"
 
# default section end
sectionEnd


# UNINSTALLER


function un.onInit
	SetShellVarContext all
 
	#Verify the uninstaller - last chance to back out
	MessageBox MB_OKCANCEL "Permanantly remove QDuply?" IDOK next
		Abort
        next:
functionEnd


section "uninstall"
 
	# Remove Start Menu launcher
	delete "$SMPROGRAMS\QDuply\QDuply.lnk"
        delete "$SMPROGRAMS\QDuply\Uninstall.lnk"
	# Try to remove the Start Menu folder - this will only happen if it is empty
	rmDir "$SMPROGRAMS\QDuply"
 
 
	# Try to remove the install directory
	rmDir /r $INSTDIR
sectionEnd